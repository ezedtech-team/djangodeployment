
# What's Covered in this Document
Deploying a Django Application with django-channels enabled using Digital Ocean Ubuntu Server

## Specifications
1. Ubuntu 20.04
1. PostgreSQL
1. Django Channels 2
1. AWS S3 or Digital Ocean Spaces
1. Run Django project as WSGI using gunicorn and systemd
1. Configure Nginx to Proxy Pass to Gunicorn and Configuring the Firewall
1. Redis Installation and Configuration
1. Deploying Django Channels with Daphne & Systemd (running the ASGI app)
    1. Starting the daphne service
    1. Writing a bash script that tells daphne to start
    1. Configuring systemd to execute bash script on server boot
1. HTTPS with [letsencrypt](https://letsencrypt.org/)


# Create Digital Ocean Droplet with SSH login

#### Create a new droplet

#### Add your domain

#### Add www A record to the domain pointing to the same IP

#### Login with ssh to root

#### Do initial server setup and create a new user
[Initial Setup](https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-20-04)

#### Login as the new user


# Install Server Dependencies
Run these commands in the SSH terminal.

`sudo apt update`

`sudo apt install python3-pip python3-dev libpq-dev postgresql postgresql-contrib nginx curl`

`sudo -u postgres psql`

`CREATE DATABASE django_db;`

`CREATE USER django WITH PASSWORD 'password';`

`ALTER ROLE django SET client_encoding TO 'utf8';`

`ALTER ROLE django SET default_transaction_isolation TO 'read committed';`

`ALTER ROLE django SET timezone TO 'UTC';`

`GRANT ALL PRIVILEGES ON DATABASE django_db TO django;`

`\q`

`sudo -H pip3 install --upgrade pip`

`sudo -H pip3 install virtualenv`

`sudo apt install git-all`

`sudo apt install libgl1-mesa-glx` Resolve cv2 issue

`mkdir Project` You can replace "Project" with your project name. 

`cd Project`

`virtualenv -p python3 env`

`source env/bin/activate`

`mkdir src`

# Get the Code on the Server

`cd src`

`git init`

`git remote add origin <url>` **Or whatever your git url is**

`git pull origin master`

`pip install -r requirements.txt`

`python manage.py collectstatic`


# Check if you can run your project (TEST)

`sudo ufw allow 8000`

`python manage.py makemigrations`

`python manage.py migrate`

`python manage.py runserver 0.0.0.0:8000`

visit [http://<your_ip_address>:8000/](http://<your_ip_address>:8000/)


# Creating systemd Socket and Service Files for Gunicorn
We've tested to see if the application will run if we run the app manually, but this isn't how we want it to be running. We want the application to run in a service so it automatically starts/restarts when it needs to. Like when we restart the server or it goes down for some reason.

One way you can do this is with gunicorn. Run this command you'll see that gunicorn can run the application:
```
gunicorn --bind 0.0.0.0:8000 Project.wsgi
```

visit [http://<your_ip_address>:8000/](http://<your_ip_address>:8000/)

So we just need a service to run that command when the server starts. One way to do that is using [systemd](https://en.wikipedia.org/wiki/Systemd)


#### Configure systemd to execute gunicorn via a `gunicorn.socket` file

`su root`

Navigate to `/etc/systemd/system/`

Create a file named: `gunicorn.socket`

Add the following to the file and save:
```
[Unit]
Description=gunicorn socket

[Socket]
ListenStream=/run/gunicorn.sock

[Install]
WantedBy=sockets.target
```

#### Create gunicorn service to run the WSGI application (the django app)
create new file: `gunicorn.service`

Add the following to `gunicorn.service` and save. **It's very important to copy this exactly as I have. Also your directory structure inside /home/django/ must be EXACTLY the same as mine. Otherwise this service file won't know what project you're talking about.**
```
[Unit]
Description=gunicorn daemon
Requires=gunicorn.socket
After=network.target

[Service]
User=django
Group=www-data
WorkingDirectory=/home/username/Project/src
ExecStart=/home/username/Project/env/bin/gunicorn \
          --access-logfile - \
          --workers 3 \
          --bind unix:/run/gunicorn.sock \
          Project.wsgi:application

[Install]
WantedBy=multi-user.target
```

`sudo systemctl start gunicorn.socket`

`sudo systemctl enable gunicorn.socket`


#### Helpful Commands
1. `sudo systemctl daemon-reload`
    - Must be executed if you change the `gunicorn.service` file.
1. `sudo systemctl restart gunicorn`
    - If you change code on your server you must execute this to see the changes take place.
1. `sudo systemctl status gunicorn`
1. `sudo shutdown -r now`
    - restart the server


#### Configure Nginx to Proxy Pass to Gunicorn
We'll be using Nginx as an HTTP Proxy. It helps to protect our website from attackers. You can read more about it here [https://docs.gunicorn.org/en/stable/deploy.html](https://docs.gunicorn.org/en/stable/deploy.html). We need to configure Nginx and gunicorn to work together.

Navigate to `/etc/nginx/sites-available`

Create file `Project`
```
server {
    server_name <your_ip_address> <yourdomain.com> <www.yourdomain.com>;

    location = /favicon.ico { access_log off; log_not_found off; }
    location /static/ {
        root /home/django/CodingWithMitchChat/src;
    }
    
     location / {
        include proxy_params;
        proxy_pass http://unix:/run/gunicorn.sock;
    }
    

}
```


Update Nginx config file at `/etc/nginx/nginx.conf` so we can upload large files (images)
```
http{
	...
	client_max_body_size 10M; 
}
```

#### Configure the Firewall
`sudo ln -s /etc/nginx/sites-available/Project /etc/nginx/sites-enabled`

`sudo nginx -t`

`sudo systemctl restart nginx`

`sudo ufw delete allow 8000`

`sudo ufw allow 'Nginx Full'`

`sudo systemctl restart gunicorn`

Set `DEBUG=False` in `settings.ini` if it isn't already.

`service gunicorn restart` (There is no difference between this command and `sudo systemctl restart gunicorn`)

Restart the server `sudo shutdown -r now`

Visit http://<your_ip_address>/


# DEBUGGING
Here are some commands you can use to look at the server logs. **These commands are absolutely crucial to know.** If your server randomly isn't working one day, this is what you use to start debugging.
1. `sudo journalctl` is where all the logs are consolidated to. That's usually where I check.
1. `sudo tail -F /var/log/nginx/error.log` View the last entries in the error log
1. `sudo journalctl -u nginx` Nginx process logs 
1. `sudo less /var/log/nginx/access.log` Nginx access logs
1. `sudo less /var/log/nginx/error.log` Nginx error logs
1. `sudo journalctl -u gunicorn` gunicorn application logs
1. `sudo journalctl -u gunicorn.socket` check gunicorn socket logs



# Install and Setup Redis
Redis is used as a kind of "messaging queue" for django channels. Read more about it here [https://channels.readthedocs.io/en/stable/topics/channel_layers.html?highlight=redis#redis-channel-layer](https://channels.readthedocs.io/en/stable/topics/channel_layers.html?highlight=redis#redis-channel-layer)

`sudo apt install redis-server`

Navigate to `/etc/redis/`

open `redis.conf`

`CTRL+F` to find 'supervised no'

change 'supervised no' to 'supervised systemd'

`SAVE`

`sudo systemctl restart redis.service`

`sudo systemctl status redis`

`CTRL+C` to exit.

`sudo apt install net-tools`

Confirm Redis is running at 127.0.0.1. Port should be 6379 by default.

`sudo netstat -lnp | grep redis`

`sudo systemctl restart redis.service`

# Deploying Django Channels with Daphne & Systemd
Gunicorn is what we use to run the WSGI application - which is our django app. To run the ASGI application we need something else, an additional tool. **[Daphne](https://github.com/django/daphne)** was built for Django channels and is the simplest. We can start daphne using a systemd service when the server boots, just like we start gunicorn and then gunicorn starts the django app.

Here are some references I found helpful. The information on this is scarce:
1. [https://channels.readthedocs.io/en/latest/deploying.html](https://channels.readthedocs.io/en/latest/deploying.html)
1. [https://stackoverflow.com/questions/50192967/deploying-django-channels-how-to-keep-daphne-running-after-exiting-shell-on-web](https://stackoverflow.com/questions/50192967/deploying-django-channels-how-to-keep-daphne-running-after-exiting-shell-on-web)

`sudo apt install daphne`

Navigate to `/etc/systemd/system/`

Create `daphne.service`. Notice the port is `8001`. This is what we need to use for our `WebSocket` connections in the templates.
```
[Unit]
Description=WebSocket Daphne Service
After=network.target

[Service]
Type=simple
User=root
WorkingDirectory=/home/django/Project/src
ExecStart=/home/django/Project/venv/bin/python /home/django/Project/venv/bin/daphne -b 0.0.0.0 -p 8001 Project.asgi:application
Restart=on-failure

[Install]
WantedBy=multi-user.target
```

`systemctl daemon-reload`

`systemctl start daphne.service`

`systemctl status daphne.service`

`CTRL+C`

# Starting the daphne Service when Server boots
With gunicorn and the WSGI application, we created a `gunicorn.socket` file that tells gunicorn to start when the server boots (at least this is my understanding). I couldn't figure out how to get this to work for daphne so instead I wrote a bash script that will run when the server boots. 

#### Create the script to run daphne
Navigate to `/root`

create `boot.sh`
```
#!/bin/sh
sudo systemctl start daphne.service
```

Save and close.

Might have to enable it to be run as a script (not sure if this is needed)
`chmod u+x /root/boot.sh`

If you want to read more about shell scripting, I found this helpful:
[https://ostechnix.com/fix-exec-format-error-when-running-scripts-with-run-parts-command/](https://ostechnix.com/fix-exec-format-error-when-running-scripts-with-run-parts-command/).


#### Tell systemd to run the bash script when the server boots

Navigate to `/etc/systemd/system`

create `on_boot.service`
```
[Service]
ExecStart=/root/boot.sh

[Install]
WantedBy=default.target
```
Save and close.

`systemctl daemon-reload`

##### Start it
`sudo systemctl start on_boot` 

##### Enable it to run at boot
`sudo systemctl enable on_boot` 

##### Allow daphne service through firewall
`ufw allow 8001` 

##### Restart the server
`sudo shutdown -r now`

##### Check the status of `on_boot.service`
`systemctl status on_boot.service`

##### Check if the daphne service started when the server started:
`systemctl status daphne.service`

#### Where are the logs?
journalctl is my general go-to. You can filter specifically for a service like this:
```
sudo journalctl -u on_boot.service // for on_boot.service
sudo journalctl -u daphne.service // for daphne.service
```

#### Update Nginx config
Earlier we configured Nginx to proxy pass to gunicorn. We need to add the new domain to that configuration.

visit `/etc/nginx/sites-available`

Update `Project`
```
server {
    server_name <your_ip_address> <yourdomain.com> <www.yourdomain.com>;

    location = /favicon.ico { access_log off; log_not_found off; }
    location /static/ {
        root /home/django/Project/src;
    }
    
     location / {
        include proxy_params;
        proxy_pass http://unix:/run/gunicorn.sock;
    }
}
```

`sudo systemctl reload nginx`

Make sure nginx configuration is still good.

`sudo nginx -t`

`service gunicorn restart`


#### Install certbot
HTTPS is a little more difficult to set up when using Django Channels. Nginx and Daphne require some extra configuring.

`sudo apt install certbot python3-certbot-nginx`

`sudo systemctl reload nginx`

Make sure nginx configuration is still good.
```
sudo nginx -t
```

#### Allow HTTPS through firewall

`sudo ufw allow 'Nginx Full'`

`sudo ufw delete allow 'Nginx HTTP'` Block standard HTTP  


#### Obtain SSL certificate

`sudo certbot --nginx -d <your-domain.whatever> -d www.<your-domain.whatever>`

For me:
```
sudo certbot --nginx -d open-chat-demo.xyz -d www.open-chat-demo.xyz
```

#### Verifying Certbot Auto-Renewal

`sudo systemctl status certbot.timer`

#### Test renewal process
`sudo certbot renew --dry-run`

## Update nginx config
We need to tell nginx to allow websocket data to move through port 8001. I'm not really sure how to explain this. I don't understand it fully. Similar to how we allow gunicorn to proxy pass nginx.

Navigate to `/etc/nginx/sites-available`

Update `Project`
```
server {

    ...
    
    location /ws/ {
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_redirect off;
        proxy_pass http://127.0.0.1:8001;
    }

    ...
}
```

## Update `daphne.service`
Tell daphne how to access our https cert.

Navigate to `/etc/systemd/system`

Update `daphne.service`
```
[Unit]
Description=WebSocket Daphne Service
After=network.target

[Service]
Type=simple
User=root
WorkingDirectory=/home/django/Project/src
ExecStart=/home/django/Project/venv/bin/python /home/django/Project/venv/bin/daphne -e ssl:8001:privateKey=/etc/letsencrypt/live/open-chat-demo.xyz/privkey.pem:certKey=/etc/letsencrypt/live/open-chat-demo.xyz/fullchain.pem Project.asgi:application  
Restart=on-failure

[Install]
WantedBy=multi-user.target
```


# Create a superuser
Before you test the server create a superuser.

`cd /home/django/Project/`

`source venv/bin/activate`

`cd src`

`python manage.py createsuperuser`


# Finishing up
Restart the server and visit your website to try it out. Everything should be working now.

# FAQ
Here are some things I wish I knew when doing this for the first time.

### If you change a file or pull a code update to the project, do you need to do anything?
Yes.

If you only change code that is *not related to django channels* then you only need to run `service gunicorn restart`.

But if you change any code related to django channels, **then you must also restart the daphne service**: `service daphne restart`.

To be safe, I always just run both. It can't hurt.
```
service gunicorn restart
service daphne restart
```
<br>

### Service Status Errors
Throughout this document we periodically check the status of the services that we set up. Things like:
1. `sudo systemctl status gunicorn`
1. `sudo systemctl status redis`
1. `systemctl status daphne.service`
1. `systemctl status on_boot.service`
1. `sudo systemctl status certbot.timer`

If any of these fail, it's not going to work and you've done something wrong. The most common problem is the directory structure does not match up. For example you might use `/home/django/django_project/src/` instead of `/home/django/CodingWithMitchChat/src/`. You need to look very carefully at your directory structures and make sure the naming is all correct and correlates with the `.service` files you build. 

When you make a change to a `.service` file, **Always run `sudo systemctl daemon-reload`**. Or to be safe, just restart the damn server `sudo shutdown -r now`. Restarting the server is the safe way, but also the slowest way. 


### CORS error in web console
You are getting an error in web console saying: "No 'Access-Control-Allow-Origin' header is present on the requested resource".

Fix this by adding CORS header in spaces settings.

# References
1. [https://www.digitalocean.com/community/tutorials/how-to-set-up-django-with-postgres-nginx-and-gunicorn-on-ubuntu-18-04](https://www.digitalocean.com/community/tutorials/how-to-set-up-django-with-postgres-nginx-and-gunicorn-on-ubuntu-18-04)
1. [https://channels.readthedocs.io/en/latest/](https://channels.readthedocs.io/en/latest/)
1. [https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-redis-on-ubuntu-20-04](https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-redis-on-ubuntu-20-04)
1. [https://www.digitalocean.com/community/tutorials/how-to-set-up-object-storage-with-django](https://www.digitalocean.com/community/tutorials/how-to-set-up-object-storage-with-django)
1. [https://stackoverflow.com/questions/61101278/how-to-run-daphne-and-gunicorn-at-the-same-time](https://stackoverflow.com/questions/61101278/how-to-run-daphne-and-gunicorn-at-the-same-time)
1. [https://github.com/conda-forge/pygridgen-feedstock/issues/10](https://github.com/conda-forge/pygridgen-feedstock/issues/10)
1. [https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-20-04](https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-20-04)